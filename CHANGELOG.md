# 1.0.0

- refactor: (BREAKING) adds variable validation and do various changes on
- tests: adds a default simple test
- chore: replaces Apache 2.0 licence to MIT
- chore: bumps pre-commit hooks

# 0.0.0

- tech: init
