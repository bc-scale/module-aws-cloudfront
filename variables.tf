variable "origin_access_identity_enabled" {
  description = "Whether to create the CloudFront Origin Access Identity or not"
  type        = bool
  default     = false
}

variable "origin_access_identity_comment" {
  description = "CloudFront origin access identities comment."
  type        = string
  default     = null
}

variable "aliases" {
  description = "Extra CNAMEs (alternate domain names), if any, for this distribution."
  type        = list(string)
  default     = []
  nullable    = false

  validation {
    error_message = "One or more of the “var.aliases” does not match “^((xn\\-\\-)?[a-z0-9\\-_]{0,61}[a-z0-9]{1,1}\\.)*(xn\\-\\-)?([a-z0-9\\-]{1,61}|[a-z0-9\\-]{1,30})\\.[a-z]{2,}$”."
    condition = length(var.aliases) == 0 || (
      alltrue([
        for v in var.aliases : can(regex("^((xn\\-\\-)?[a-z0-9\\-_]{0,61}[a-z0-9]{1,1}\\.)*(xn\\-\\-)?([a-z0-9\\-]{1,61}|[a-z0-9\\-]{1,30})\\.[a-z]{2,}$", v))
      ])
    )
  }
}

variable "comment" {
  description = "Any comments you want to include about the distribution."
  type        = string
  default     = null
}

variable "default_root_object" {
  description = "The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL."
  type        = string
  default     = null
}

variable "enabled" {
  description = "Whether the distribution is enabled to accept end user requests for content."
  type        = bool
  default     = true
}

variable "http_version" {
  description = "The maximum HTTP version to support on the distribution. Allowed values are http1.1, http2, http2and3 and http3. The default is http3."
  type        = string
  default     = "http3"

  validation {
    error_message = "The “var.http_version” must be one of “http1.1”, “http2”, “http2and3” or “http3”."
    condition     = var.http_version == null || try(contains(["http1.1", "http2", "http2and3", "http3"], var.http_version), false)
  }
}

variable "ipv6_enabled" {
  description = "Whether to enable IPv6 or not for the distribution."
  type        = bool
  default     = false
}

variable "price_class" {
  description = "The price class for this distribution. One of PriceClass_All, PriceClass_200, PriceClass_100"
  type        = string
  default     = null

  validation {
    error_message = "The “var.price_class” must be one of “PriceClass_All”, “PriceClass_200” or “PriceClass_100”."
    condition     = var.price_class == null || try(contains(["PriceClass_All", "PriceClass_200", "PriceClass_100"], var.price_class), false)
  }
}

variable "retain_on_delete" {
  description = "Whether tp disables the distribution instead of deleting it when destroying the resource through Terraform or not. If this is set, the distribution needs to be deleted manually afterwards."
  type        = bool
  default     = false
}

variable "wait_for_deployment" {
  description = "Whether to wait for the distribution status to change from InProgress to Deployed or not"
  type        = bool
  default     = true
}

variable "web_acl_id" {
  description = "If you're using AWS WAF to filter CloudFront requests, the Id of the AWS WAF web ACL that is associated with the distribution. The WAF Web ACL must exist in the WAF Global (CloudFront) region and the credentials configuring this argument must have waf:GetWebACL permissions assigned. If using WAFv2, provide the ARN of the web ACL."
  type        = string
  default     = null

  validation {
    error_message = "The “var.web_acl_id” must match “^(arn:aws(-us-gov|-cn)?:wafv2:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):global/webacl/[a-zA-Z0-9-]{1,128}/)?[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}$”."
    condition     = var.web_acl_id == null || can(regex("^(arn:aws(-us-gov|-cn)?:wafv2:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):global/webacl/[a-zA-Z0-9-]{1,128}/)?[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}$", var.web_acl_id))
  }
}

variable "tags" {
  description = "A map of tags to assign to the resource."
  type        = map(string)
  default     = {}
}

variable "origin" {
  description = "One or more origins for this distribution (multiples allowed)."
  default     = []
  type = list(object({
    domain_name              = string
    origin_id                = string
    origin_path              = optional(string, "")
    connection_attempts      = optional(number, 3)
    connection_timeout       = optional(number, 10)
    origin_access_control_id = optional(string)
    s3_origin_config = optional(object({
      origin_access_identity = optional(string, null)
    }), null)
    custom_origin_config = optional(object({
      http_port                = optional(number, 80)
      https_port               = optional(number, 443)
      origin_protocol_policy   = optional(string, "https-only")
      origin_ssl_protocols     = optional(list(string), ["TLSv1.2"])
      origin_keepalive_timeout = optional(number, 5)
      origin_read_timeout      = optional(number, 30)
    }), null)
    custom_header = optional(list(object({
      name  = optional(string, "")
      value = optional(string, "")
    })), [])
    origin_shield = optional(object({
      enabled              = optional(bool, true)
      origin_shield_region = optional(string, "us-east-1")
    }), null)
  }))

  validation {
    error_message = "One or more “var.origin” are invalid. Check the requirements in the variables.tf file."
    condition = length(var.origin) == 0 || (alltrue([
      for origin_value in var.origin : (
        can(regex("^((xn\\-\\-)?[a-z0-9\\-_]{0,61}[a-z0-9]{1,1}\\.)*(xn\\-\\-)?([a-z0-9\\-]{1,61}|[a-z0-9\\-]{1,30})\\.[a-z]{2,}$", origin_value.domain_name)) &&
        contains(range(1, 4), origin_value.connection_attempts) &&
        contains(range(1, 11), origin_value.connection_timeout) &&
        (
          origin_value.custom_origin_config == null ||
          (
            try(origin_value.custom_origin_config.http_port, 1) > 0 &&
            try(origin_value.custom_origin_config.http_port, 1) < 65536 &&
            try(origin_value.custom_origin_config.https_port, 1) > 0 &&
            try(origin_value.custom_origin_config.https_port, 1) < 65536 &&
            contains(["http-only", "https-only", "match-viewer"], try(origin_value.custom_origin_config.origin_protocol_policy, "https-only")) &&
            alltrue([for v in try(origin_value.custom_origin_config.origin_ssl_protocols, []) : contains(["SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2"], v)]) &&
            try(origin_value.custom_origin_config.origin_keepalive_timeout, 1) > 0 &&
            try(origin_value.custom_origin_config.origin_keepalive_timeout, 1) < 61 &&
            try(origin_value.custom_origin_config.origin_keepalive_timeout, 1) > 0 &&
            try(origin_value.custom_origin_config.origin_keepalive_timeout, 1) < 61
          )
        ) &&
        (
          origin_value.origin_shield == null ||
          can(regex("^(af|ap|ca|eu|me|sa|us)-(central|north|(north(?:east|west))|south|south(?:east|west)|east|west)-[0-9]$", try(origin_value.origin_shield.origin_shield_region, "us-east-1")))
        )
      )
      ])
    )
  }
}

variable "origin_groups" {
  description = "One or more origin_group for this distribution (multiples allowed)."

  type = list(object({
    origin_id                  = string
    failover_status_codes      = list(number)
    primary_member_origin_id   = string
    secondary_member_origin_id = string
  }))
  default = []

  validation {
    error_message = "One or more “var.origin” are invalid. Check the requirements in the variables.tf file."
    condition = (
      length(var.origin_groups) == 0 || (
        alltrue([
          for origin_group in var.origin_groups : (
            (alltrue([for failover_status_code in origin_group.failover_status_codes : (failover_status_code > 99 && failover_status_code < 600)]))
          )
        ])
      )
    )
  }
}

variable "viewer_certificate" {
  description = "The SSL configuration for this distribution"
  type = object({
    acm_certificate_arn            = optional(string)
    cloudfront_default_certificate = optional(bool, false)
    iam_certificate_id             = optional(string)

    minimum_protocol_version = optional(string)
    ssl_support_method       = optional(string)
  })
  default = {
    cloudfront_default_certificate = true
  }

  validation {
    error_message = "One or more “var.viewer_certificate” are invalid. Check the requirements in the variables.tf file."
    condition = (
      var.viewer_certificate == null || (
        try(var.viewer_certificate.cloudfront_default_certificate, false) || (
          (
            can(regex("^arn:(aws[a-zA-Z-]*)?:acm:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-[0-9]{1}:[0-9]{12}:certificate/[a-zA-Z0-9-_]+$", coalesce(var.viewer_certificate.acm_certificate_arn, ""))) ||
            can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws):server-certificate/[a-zA-Z0-9+=,\\./@-]+$", coalesce(var.viewer_certificate.cloudfront_default_certificate, "")))
          ) &&
          (
            (
              try(contains(["SSLv3", "TLSv1", "TLSv1_2016", "TLSv1.1_2016", "TLSv1.2_2018", "TLSv1.2_2019", "TLSv1.2_2021"], var.viewer_certificate.minimum_protocol_version), false) &&
              try(contains(["sni-only", "vip"], var.viewer_certificate.ssl_support_method), false)
            )
          )
        )
      )
    )
  }
}

variable "geo_restriction" {
  description = "The restriction configuration for this distribution (geo_restrictions)"
  type = object({
    restriction_type = string
    locations        = optional(list(string), [])
  })
  default = {
    restriction_type = "none"
  }

  validation {
    error_message = "One or more “var.geo_restriction” are invalid. Check the requirements in the variables.tf file."
    condition = (
      contains(["none", "whitelist", "blacklist"], coalesce(var.geo_restriction.restriction_type, "none")) &&
      alltrue([
        for location in try(var.geo_restriction.locations, []) : can(regex("^[A-Z]{2}$", location))
      ])
    )
  }
}

variable "logging_config" {
  description = "The logging configuration that controls how logs are written to your distribution (maximum one)."
  type = object({
    bucket          = string
    prefix          = optional(string)
    include_cookies = optional(bool, false)
  })
  default = null
}

variable "custom_error_response" {
  description = "One or more custom error response elements"
  type = list(object({
    error_code = number

    response_code         = optional(number)
    response_page_path    = optional(string)
    error_caching_min_ttl = optional(number)
  }))
  default = []

  validation {
    error_message = "One or more “var.geo_restriction” are invalid. Check the requirements in the variables.tf file."
    condition = length(var.custom_error_response) == 0 || (
      alltrue([for v in var.custom_error_response : (
        v.error_code > 399 &&
        v.error_code < 600 &&
        (
          coalesce(v.response_code, 400) > 399 &&
          coalesce(v.response_code, 400) < 600
        )
      )])
    )
  }
}

variable "default_cache_behavior" {
  description = "The default cache behavior for this distribution"
  type = object({
    target_origin_id           = string
    viewer_protocol_policy     = string
    allowed_methods            = list(string)
    cached_methods             = list(string)
    compress                   = optional(bool, false)
    field_level_encryption_id  = optional(string)
    smooth_streaming           = optional(bool, false)
    trusted_signers            = optional(list(string), [])
    trusted_key_groups         = optional(list(string), [])
    cache_policy_id            = optional(string)
    origin_request_policy_id   = optional(string)
    response_headers_policy_id = optional(string)
    realtime_log_config_arn    = optional(string)
    min_ttl                    = optional(number)
    default_ttl                = optional(number)
    max_ttl                    = optional(number)
    forwarded_values = optional(object({
      query_string            = string
      query_string_cache_keys = optional(list(string), [])
      headers                 = optional(list(string), [])
      cookies = optional(object({
        forward           = string
        whitelisted_names = optional(list(string), [])
      }), null)
    }), null)
    lambda_function_associations = optional(list(object({
      event_type   = string
      lambda_arn   = string
      include_body = optional(bool, false)
    })), [])
    function_associations = optional(list(object({
      event_type   = string
      function_arn = string
    })), [])
  })
  default = null

  validation {
    error_message = "One or more “var.viewer_certificate” are invalid. Check the requirements in the variables.tf file."
    condition = var.default_cache_behavior == null || try(
      alltrue([for method in var.default_cache_behavior.allowed_methods : contains(["GET", "HEAD", "OPTIONS", "PUT", "PATCH", "POST", "DELETE"], method)]) &&
      alltrue([for method in var.default_cache_behavior.cached_methods : contains(["GET", "HEAD", "OPTIONS", "PUT", "PATCH", "POST", "DELETE"], method)]) &&
      alltrue([for trusted_signer in var.default_cache_behavior.trusted_signers : can(regex("^((self)|[0-9]{12})$", trusted_signer))]) &&
      (
        var.default_cache_behavior.realtime_log_config_arn == null ||
        can(regex("^arn:(aws[a-zA-Z-]*)?:cloudfront:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-[0-9]{1}:[0-9]{12}:realtime-log-config/[a-zA-Z0-9-_]+", coalesce(var.default_cache_behavior.realtime_log_config_arn, "")))
      ) &&
      coalesce(var.default_cache_behavior.min_ttl, 0) >= 0 &&
      coalesce(var.default_cache_behavior.default_ttl, 0) >= 0 &&
      coalesce(var.default_cache_behavior.max_ttl, 0) >= 0 &&
      (var.default_cache_behavior.forwarded_values == null || contains(["all", "none", "whitelist"], try(var.default_cache_behavior.forwarded_values.cookies.forward, "all"))) &&
      alltrue([for lambda_function_association in coalesce(var.default_cache_behavior.lambda_function_associations, []) : (
        contains(["viewer-request", "origin-request", "viewer-response", "origin-response"], lambda_function_association.event_type) &&
        can(regex("^^arn:(aws[a-zA-Z-]*)?:lambda::[0-9]{12}:function:.+$", lambda_function_association.lambda_arn))
      )]) &&
      alltrue([for function_association in coalesce(var.default_cache_behavior.function_associations, []) : (
        contains(["viewer-request", "viewer-response"], function_association.event_type) &&
        can(regex("^^arn:(aws[a-zA-Z-]*)?:cloudfront::[0-9]{12}:function/[a-zA-Z0-9-_]{1,64}$", function_association.function_arn))
        )]
      ),
    false)
  }
}

variable "ordered_cache_behaviors" {
  description = "An ordered list of cache behaviors resource for this distribution. List from top to bottom in order of precedence. The topmost cache behavior will have precedence 0."
  type = list(
    object({
      path_pattern               = string
      target_origin_id           = string
      viewer_protocol_policy     = string
      allowed_methods            = list(string)
      cached_methods             = list(string)
      compress                   = optional(bool, false)
      field_level_encryption_id  = optional(string)
      smooth_streaming           = optional(bool, false)
      trusted_signers            = optional(list(string), [])
      trusted_key_groups         = optional(list(string), [])
      cache_policy_id            = optional(string)
      origin_request_policy_id   = optional(string)
      response_headers_policy_id = optional(string)
      realtime_log_config_arn    = optional(string)
      min_ttl                    = optional(number)
      default_ttl                = optional(number)
      max_ttl                    = optional(number)
      forwarded_values = optional(object({
        query_string            = string
        query_string_cache_keys = optional(list(string), [])
        headers                 = optional(list(string), [])
        cookies = optional(object({
          forward           = string
          whitelisted_names = optional(list(string), [])
        }), null)
      }), null)
      lambda_function_associations = optional(list(object({
        event_type   = string
        lambda_arn   = string
        include_body = optional(bool, false)
      })), [])
      function_associations = optional(list(object({
        event_type   = string
        function_arn = string
      })), [])
    })
  )
  default = []

  validation {
    error_message = "One or more “var.ordered_cache_behaviors” are invalid. Check the requirements in the variables.tf file."
    condition = length(var.ordered_cache_behaviors) == 0 || try(
      alltrue([for cache_behavior in var.ordered_cache_behaviors : (
        alltrue([for method in cache_behavior.allowed_methods : contains(["GET", "HEAD", "OPTIONS", "PUT", "PATCH", "POST", "DELETE"], method)]) &&
        alltrue([for method in cache_behavior.cached_methods : contains(["GET", "HEAD", "OPTIONS", "PUT", "PATCH", "POST", "DELETE"], method)]) &&
        alltrue([for trusted_signer in cache_behavior.trusted_signers : can(regex("^((self)|[0-9]{12})$", trusted_signer))]) &&
        (
          cache_behavior.realtime_log_config_arn == null ||
          can(regex("^arn:(aws[a-zA-Z-]*)?:cloudfront:[a-z]{2}((-gov)|(-iso(b?)))?-[a-z]+-[0-9]{1}:[0-9]{12}:realtime-log-config/[a-zA-Z0-9-_]+", coalesce(cache_behavior.realtime_log_config_arn, "")))
        ) &&
        coalesce(cache_behavior.min_ttl, 0) >= 0 &&
        coalesce(cache_behavior.default_ttl, 0) >= 0 &&
        coalesce(cache_behavior.max_ttl, 0) >= 0 &&
        (cache_behavior.forwarded_values == null || contains(["all", "none", "whitelist"], try(cache_behavior.forwarded_values.cookies.forward, "all"))) &&
        alltrue([for lambda_function_association in coalesce(cache_behavior.lambda_function_associations, []) : (
          contains(["viewer-request", "origin-request", "viewer-response", "origin-response"], lambda_function_association.event_type) &&
          can(regex("^^arn:(aws[a-zA-Z-]*)?:lambda::[0-9]{12}:function:.+$", lambda_function_association.lambda_arn))
        )]) &&
        alltrue([for function_association in coalesce(cache_behavior.function_associations, []) : (
          contains(["viewer-request", "viewer-response"], function_association.event_type) &&
          can(regex("^^arn:(aws[a-zA-Z-]*)?:cloudfront::[0-9]{12}:function/[a-zA-Z0-9-_]{1,64}$", function_association.function_arn))
        )])
        )
      ]), false
    )
  }
}

variable "monitoring_subscription_enabled" {
  description = "Whether to create the monitoring subscription or not."
  type        = bool
  default     = false
}

variable "realtime_metrics_subscription_status" {
  description = "A flag that indicates whether additional CloudWatch metrics are enabled for a given CloudFront distribution. Valid values are `Enabled` and `Disabled`."
  type        = string
  default     = "Enabled"

  validation {
    error_message = "The “var.realtime_metrics_subscription_status” must be “Enabled” or “Disabled”."
    condition     = contains(["Enabled", "Disabled"], var.realtime_metrics_subscription_status)
  }
}
