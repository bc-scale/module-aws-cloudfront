output "distribution_id" {
  value = try(aws_cloudfront_distribution.this["0"].id, null)
}

output "distribution_arn" {
  value = try(aws_cloudfront_distribution.this["0"].arn, null)
}

output "distribution_caller_reference" {
  value = try(aws_cloudfront_distribution.this["0"].caller_reference, null)
}

output "distribution_status" {
  value = try(aws_cloudfront_distribution.this["0"].status, null)
}

output "distribution_trusted_signers" {
  value = try(aws_cloudfront_distribution.this["0"].trusted_signers, null)
}

output "distribution_domain_name" {
  value = try(aws_cloudfront_distribution.this["0"].domain_name, null)
}

output "distribution_last_modified_time" {
  value = try(aws_cloudfront_distribution.this["0"].last_modified_time, null)
}

output "distribution_in_progress_validation_batches" {
  value = try(aws_cloudfront_distribution.this["0"].in_progress_validation_batches, null)
}

output "distribution_etag" {
  value = try(aws_cloudfront_distribution.this["0"].etag, null)
}

output "distribution_hosted_zone_id" {
  value = try(aws_cloudfront_distribution.this["0"].hosted_zone_id, null)
}

output "origin_access_identity_id" {
  value = try(aws_cloudfront_origin_access_identity.this["0"].id, null)
}

output "origin_access_identity_iam_arn" {
  value = try(aws_cloudfront_origin_access_identity.this["0"].iam_arn, null)
}

output "monitoring_subscription_id" {
  value = try(aws_cloudfront_monitoring_subscription.this[0].id, null)
}
