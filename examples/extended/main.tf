#####
# Randoms
#####

resource "random_string" "this" {
  length  = 4
  upper   = false
  numeric = false
  special = false
}

locals {
  prefix = "${random_string.this.result}-tftest"
}

#####
# Baseline
#####

resource "aws_s3_bucket" "this" {
  bucket        = "${local.prefix}-website"
  force_destroy = true
}

resource "aws_s3_bucket" "log" {
  bucket        = "${local.prefix}-logs"
  force_destroy = true
}


data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.this.arn}/static/*"]

    principals {
      type        = "AWS"
      identifiers = [module.extended.origin_access_identity_iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.this.id
  policy = data.aws_iam_policy_document.s3_policy.json
}

resource "aws_cloudfront_function" "this" {
  name    = "${local.prefix}-cf-function"
  runtime = "cloudfront-js-1.0"
  code    = file("example-function.js")
}

data "aws_cloudfront_response_headers_policy" "this" {
  name = "Managed-SimpleCORS"
}

#####
# Extended cloudfront
# For CI reason, it does not included:
#  - Custom certificate
#  - lambda@Edge function
#####

module "extended" {
  source = "../../"

  aliases = []

  comment             = "My awesome CloudFront"
  enabled             = true
  ipv6_enabled        = true
  price_class         = "PriceClass_All"
  retain_on_delete    = false
  wait_for_deployment = false

  monitoring_subscription_enabled = true

  origin_access_identity_enabled = true
  origin_access_identity_comment = "TF test"

  logging_config = {
    bucket = aws_s3_bucket.log.bucket_regional_domain_name
    prefix = "cloudfront"
  }

  origin = [{
    origin_id   = "foo"
    domain_name = "appsync.foo"
    custom_origin_config = {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "match-viewer"
      origin_ssl_protocols   = ["TLSv1", "TLSv1.1", "TLSv1.2"]
    }

    custom_header = [
      {
        name  = "X-Forwarded-Scheme"
        value = "https"
      },
      {
        name  = "X-Frame-Options"
        value = "SAMEORIGIN"
      }
    ]

    origin_shield = {
      enabled              = true
      origin_shield_region = "us-east-1"
    }
    },
    {
      origin_id   = "bar"
      domain_name = aws_s3_bucket.this.bucket_regional_domain_name
      s3_origin_config = {
        origin_access_identity = "self"
      }
    }
  ]

  origin_groups = [
    {
      origin_id                  = "first"
      failover_status_codes      = [403, 404, 500, 502]
      primary_member_origin_id   = "foo"
      secondary_member_origin_id = "bar"
    }
  ]

  default_cache_behavior = {
    target_origin_id       = "foo"
    viewer_protocol_policy = "allow-all"
    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
    cached_methods         = ["GET", "HEAD"]
    compress               = true
    query_string           = true

    response_headers_policy_id = data.aws_cloudfront_response_headers_policy.this.id

    forwarded_values = {
      query_string = false

      cookies = {
        forward = "none"
      }
    }

    lambda_function_association = {}
  }

  ordered_cache_behaviors = [
    {
      path_pattern           = "/static/*"
      target_origin_id       = "bar"
      viewer_protocol_policy = "redirect-to-https"

      allowed_methods = ["GET", "HEAD", "OPTIONS"]
      cached_methods  = ["GET", "HEAD"]
      compress        = true
      query_string    = true

      forwarded_values = {
        query_string = false

        cookies = {
          forward = "none"
        }
      }

      function_associations = [{
        event_type   = "viewer-request"
        function_arn = aws_cloudfront_function.this.arn
        },
        {
          event_type   = "viewer-response"
          function_arn = aws_cloudfront_function.this.arn
        }
      ]
    }
  ]

  custom_error_response = [{
    error_code         = 404
    response_code      = 404
    response_page_path = "/errors/404.html"
    }, {
    error_code         = 403
    response_code      = 403
    response_page_path = "/errors/403.html"
  }]

  geo_restriction = {
    restriction_type = "whitelist"
    locations        = ["NO", "UA", "US", "GB"]
  }

}
