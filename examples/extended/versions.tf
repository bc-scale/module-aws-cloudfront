terraform {
  required_version = ">= 1.3.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.29"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 2.0"
    }
  }
}
