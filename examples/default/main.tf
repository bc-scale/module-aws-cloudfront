#####
# Randoms
#####

resource "random_string" "this" {
  length  = 4
  upper   = false
  numeric = false
  special = false
}

locals {
  prefix = "${random_string.this.result}-tftest"
}

#####
# Baseline
#####

resource "aws_s3_bucket" "this" {
  bucket        = "${local.prefix}-website"
  force_destroy = true
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.this.arn}/static/*"]

    principals {
      type        = "AWS"
      identifiers = [module.default.origin_access_identity_iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.this.id
  policy = data.aws_iam_policy_document.s3_policy.json
}

#####
# Default cloudfront with S3 bucket
#####

module "default" {
  source = "../../"

  comment             = "My awesome CloudFront"
  price_class         = "PriceClass_All"
  wait_for_deployment = false

  origin_access_identity_enabled = true
  origin_access_identity_comment = "TF test"

  origin = [
    {
      origin_id   = "foo"
      domain_name = aws_s3_bucket.this.bucket_regional_domain_name
      s3_origin_config = {
        origin_access_identity = "self"
      }
    }
  ]

  default_cache_behavior = {
    target_origin_id       = "foo"
    viewer_protocol_policy = "allow-all"
    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
    cached_methods         = ["GET", "HEAD"]

    forwarded_values = {
      query_string = false

      cookies = {
        forward = "none"
      }
    }
  }
}
