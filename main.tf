locals {
  tags = {
    managed-by = "Terraform"
  }
}

resource "aws_cloudfront_distribution" "this" {
  for_each = { 0 = 0 }

  aliases             = var.aliases
  comment             = var.comment
  default_root_object = var.default_root_object
  enabled             = var.enabled
  http_version        = var.http_version
  is_ipv6_enabled     = var.ipv6_enabled
  price_class         = var.price_class
  retain_on_delete    = var.retain_on_delete
  wait_for_deployment = var.wait_for_deployment
  web_acl_id          = var.web_acl_id
  tags = merge(
    local.tags,
    var.tags,
  )

  dynamic "logging_config" {
    for_each = var.logging_config == null ? {} : { 0 = 0 }

    content {
      bucket          = var.logging_config.bucket
      prefix          = var.logging_config.prefix
      include_cookies = var.logging_config.include_cookies
    }
  }

  dynamic "origin" {
    for_each = var.origin

    content {
      domain_name              = origin.value.domain_name
      origin_id                = origin.value.origin_id
      origin_path              = origin.value.origin_path
      connection_attempts      = origin.value.connection_attempts
      connection_timeout       = origin.value.connection_timeout
      origin_access_control_id = origin.value.origin_access_control_id

      dynamic "s3_origin_config" {
        for_each = origin.value.s3_origin_config == null ? {} : { 0 = 0 }

        content {
          origin_access_identity = (
            origin.value.s3_origin_config.origin_access_identity == "self" ||
            origin.value.s3_origin_config.origin_access_identity == null
          ) ? try(aws_cloudfront_origin_access_identity.this["0"].cloudfront_access_identity_path, null) : origin.value.s3_origin_config.origin_access_identity
        }
      }

      dynamic "custom_origin_config" {
        for_each = origin.value.custom_origin_config == null ? {} : { 0 = 0 }

        content {
          http_port                = origin.value.custom_origin_config.http_port
          https_port               = origin.value.custom_origin_config.https_port
          origin_protocol_policy   = origin.value.custom_origin_config.origin_protocol_policy
          origin_ssl_protocols     = origin.value.custom_origin_config.origin_ssl_protocols
          origin_keepalive_timeout = origin.value.custom_origin_config.origin_keepalive_timeout
          origin_read_timeout      = origin.value.custom_origin_config.origin_read_timeout
        }
      }

      dynamic "custom_header" {
        for_each = origin.value.custom_header

        content {
          name  = custom_header.value.name
          value = custom_header.value.value
        }
      }

      dynamic "origin_shield" {
        for_each = origin.value.origin_shield == null ? {} : { 0 = 0 }

        content {
          enabled              = origin.value.origin_shield.enabled
          origin_shield_region = origin.value.origin_shield.origin_shield_region
        }
      }
    }
  }

  dynamic "origin_group" {
    for_each = var.origin_groups

    content {
      origin_id = origin_group.value.origin_id

      failover_criteria {
        status_codes = origin_group.value.failover_status_codes
      }

      member {
        origin_id = origin_group.value.primary_member_origin_id
      }

      member {
        origin_id = origin_group.value.secondary_member_origin_id
      }
    }
  }

  dynamic "default_cache_behavior" {
    for_each = var.default_cache_behavior == null ? {} : { 0 = 0 }

    content {
      target_origin_id       = var.default_cache_behavior.target_origin_id
      viewer_protocol_policy = var.default_cache_behavior.viewer_protocol_policy

      allowed_methods           = var.default_cache_behavior.allowed_methods
      cached_methods            = var.default_cache_behavior.cached_methods
      compress                  = var.default_cache_behavior.compress
      field_level_encryption_id = var.default_cache_behavior.field_level_encryption_id
      smooth_streaming          = var.default_cache_behavior.smooth_streaming
      trusted_signers           = var.default_cache_behavior.trusted_signers
      trusted_key_groups        = var.default_cache_behavior.trusted_key_groups

      cache_policy_id            = var.default_cache_behavior.cache_policy_id
      origin_request_policy_id   = var.default_cache_behavior.origin_request_policy_id
      response_headers_policy_id = var.default_cache_behavior.response_headers_policy_id
      realtime_log_config_arn    = var.default_cache_behavior.realtime_log_config_arn

      min_ttl     = var.default_cache_behavior.min_ttl
      default_ttl = var.default_cache_behavior.default_ttl
      max_ttl     = var.default_cache_behavior.max_ttl

      dynamic "forwarded_values" {
        for_each = var.default_cache_behavior.forwarded_values == null ? {} : { 0 = 0 }

        content {
          query_string            = var.default_cache_behavior.forwarded_values.query_string
          query_string_cache_keys = var.default_cache_behavior.forwarded_values.query_string_cache_keys
          headers                 = var.default_cache_behavior.forwarded_values.headers

          cookies {
            forward           = var.default_cache_behavior.forwarded_values.cookies.forward
            whitelisted_names = var.default_cache_behavior.forwarded_values.cookies.whitelisted_names
          }
        }
      }

      dynamic "lambda_function_association" {
        for_each = var.default_cache_behavior.lambda_function_associations

        content {
          event_type   = lambda_function_association.value.event_type
          lambda_arn   = lambda_function_association.value.lambda_arn
          include_body = lambda_function_association.value.include_body
        }
      }

      dynamic "function_association" {
        for_each = var.default_cache_behavior.function_associations

        content {
          event_type   = function_association.value.event_type
          function_arn = function_association.value.function_arn
        }
      }
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.ordered_cache_behaviors

    content {
      path_pattern           = ordered_cache_behavior.value.path_pattern
      target_origin_id       = ordered_cache_behavior.value.target_origin_id
      viewer_protocol_policy = ordered_cache_behavior.value.viewer_protocol_policy

      allowed_methods           = ordered_cache_behavior.value.allowed_methods
      cached_methods            = ordered_cache_behavior.value.cached_methods
      compress                  = ordered_cache_behavior.value.compress
      field_level_encryption_id = ordered_cache_behavior.value.field_level_encryption_id
      smooth_streaming          = ordered_cache_behavior.value.smooth_streaming
      trusted_signers           = ordered_cache_behavior.value.trusted_signers
      trusted_key_groups        = ordered_cache_behavior.value.trusted_key_groups

      cache_policy_id            = ordered_cache_behavior.value.cache_policy_id
      origin_request_policy_id   = ordered_cache_behavior.value.origin_request_policy_id
      response_headers_policy_id = ordered_cache_behavior.value.response_headers_policy_id
      realtime_log_config_arn    = ordered_cache_behavior.value.realtime_log_config_arn

      min_ttl     = ordered_cache_behavior.value.min_ttl
      default_ttl = ordered_cache_behavior.value.default_ttl
      max_ttl     = ordered_cache_behavior.value.max_ttl

      dynamic "forwarded_values" {
        for_each = ordered_cache_behavior.value.forwarded_values == null ? {} : { 0 = 0 }

        content {
          query_string            = ordered_cache_behavior.value.forwarded_values.query_string
          query_string_cache_keys = ordered_cache_behavior.value.forwarded_values.query_string_cache_keys
          headers                 = ordered_cache_behavior.value.forwarded_values.headers

          cookies {
            forward           = ordered_cache_behavior.value.forwarded_values.cookies.forward
            whitelisted_names = ordered_cache_behavior.value.forwarded_values.cookies.whitelisted_names
          }
        }
      }

      dynamic "lambda_function_association" {
        for_each = ordered_cache_behavior.value.lambda_function_associations

        content {
          event_type   = lambda_function_association.value.event_type
          lambda_arn   = lambda_function_association.value.lambda_arn
          include_body = lookup(lambda_function_association.value, "include_body", null)
        }
      }

      dynamic "function_association" {
        for_each = ordered_cache_behavior.value.function_associations

        content {
          event_type   = function_association.value.event_type
          function_arn = function_association.value.function_arn
        }
      }
    }
  }

  dynamic "viewer_certificate" {
    for_each = var.viewer_certificate == null ? {} : { 0 = 0 }

    content {
      acm_certificate_arn            = var.viewer_certificate.acm_certificate_arn
      cloudfront_default_certificate = var.viewer_certificate.cloudfront_default_certificate
      iam_certificate_id             = var.viewer_certificate.iam_certificate_id

      minimum_protocol_version = var.viewer_certificate.minimum_protocol_version
      ssl_support_method       = var.viewer_certificate.ssl_support_method
    }
  }

  dynamic "custom_error_response" {
    for_each = var.custom_error_response

    content {
      error_code = custom_error_response.value.error_code

      response_code         = custom_error_response.value.response_code
      response_page_path    = custom_error_response.value.response_page_path
      error_caching_min_ttl = custom_error_response.value.error_caching_min_ttl
    }
  }

  dynamic "restrictions" {
    for_each = var.geo_restriction == null ? {} : { 0 = 0 }

    content {
      geo_restriction {
        restriction_type = var.geo_restriction.restriction_type
        locations        = var.geo_restriction.locations
      }
    }
  }
}

resource "aws_cloudfront_origin_access_identity" "this" {
  for_each = var.origin_access_identity_enabled == false ? {} : { 0 = 0 }

  comment = var.origin_access_identity_comment

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_cloudfront_monitoring_subscription" "this" {
  for_each = var.monitoring_subscription_enabled == false ? {} : { 0 = 0 }

  distribution_id = aws_cloudfront_distribution.this["0"].id

  monitoring_subscription {
    realtime_metrics_subscription_config {
      realtime_metrics_subscription_status = var.realtime_metrics_subscription_status
    }
  }
}
